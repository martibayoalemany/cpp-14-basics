//
// Created by username on 10.05.17.
//

#ifndef CPP14_BASICS_UTILS_H
#define CPP14_BASICS_UTILS_H


class Utils {

    public:

    static void doInvalidMemoryAccess();
    static void doMultithreading();    
    static void setSignalHandler();
};


#endif //CPP14_BASICS_UTILS_H
